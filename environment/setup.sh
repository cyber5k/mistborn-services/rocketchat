#!/bin/bash

figlet "Mistborn: Rocket.Chat Credentials"

# generate rocketchat .env files
ROCKETCHAT_PROD_FILE="./.envs/.production/.rocketchat"
#ROCKETCHAT_PASSWORD=$(python3 -c "import secrets; import string; print(f''.join([secrets.choice(string.ascii_letters+string.digits) for x in range(32)]))")
ROCKETCHAT_PASSWORD="$1"
echo "ROCKETCHAT_USER=bot" > $ROCKETCHAT_PROD_FILE
echo "ROCKETCHAT_ROOM=GENERAL" >> $ROCKETCHAT_PROD_FILE
echo "BOT_NAME=bot" >> $ROCKETCHAT_PROD_FILE
echo "ROCKETCHAT_PASSWORD=$ROCKETCHAT_PASSWORD" >> $ROCKETCHAT_PROD_FILE

